package com.lionware.newsarticles.di

import com.lionware.newsarticles.ui.articles.ArticlesViewModelFactory
import com.lionware.newsarticles.ui.articles.ArticlesViewModelFactoryImpl
import com.lionware.newsarticles.ui.articles.GetArticlesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {

    @Provides
    fun provideArticlesViewModelFactory(getArticlesUseCase: GetArticlesUseCase): ArticlesViewModelFactory =
        ArticlesViewModelFactoryImpl(getArticlesUseCase)
}