package com.lionware.newsarticles.ui.articles

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lionware.newsarticles.R
import com.lionware.newsarticles.data.entities.Article
import com.lionware.newsarticles.data.remote.NewsRepository
import com.lionware.newsarticles.utils.Resource
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetArticlesUseCase @Inject constructor(
    @ApplicationContext private val appContext: Context,
    private val newsRepository: NewsRepository
) {
    private val _articles = MutableLiveData<Resource<List<Article>>>()
    val articles: LiveData<Resource<List<Article>>>
        get() = _articles

    suspend fun get(query: String, from: String, sortBy: String) {
        withContext(Dispatchers.Main) {
            _articles.value = Resource.loading()
        }
        val response = newsRepository.getArticles(query, from, sortBy)
        if (response.status == Resource.Status.SUCCESS && response.data != null) {
            _articles.postValue(Resource.success(response.data.articles))
        } else {
            _articles.postValue(Resource.error(appContext.getString(R.string.network_error)))
        }
    }
}