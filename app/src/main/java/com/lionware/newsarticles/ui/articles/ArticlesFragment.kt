package com.lionware.newsarticles.ui.articles

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.lionware.newsarticles.R
import com.lionware.newsarticles.databinding.FragmentArticlesBinding
import com.lionware.newsarticles.utils.Resource
import com.lionware.newsarticles.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ArticlesFragment : Fragment(R.layout.fragment_articles) {

    @Inject
    lateinit var viewModelFactory: ArticlesViewModelFactory
    private lateinit var viewModel: ArticlesViewModel
    private lateinit var articlesAdapter: ArticlesAdapter
    private var binding: FragmentArticlesBinding by autoCleared()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentArticlesBinding.bind(view)
        setUpRecyclerView()
        setUpViewModel()
        fetchArticles()
    }

    private fun setUpRecyclerView() {
        articlesAdapter = ArticlesAdapter()
        binding.recyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = articlesAdapter
        }
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(ArticlesViewModel::class.java)
        viewModel.articles.observe(viewLifecycleOwner) {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!it.data.isNullOrEmpty()) {
                        articlesAdapter.setItems(ArrayList(it.data))
                    }
                }
                Resource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
                Resource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun fetchArticles() {
        viewModel.fetchArticles("bitcoin", "2021-01-15", "publishedAt")
    }
}