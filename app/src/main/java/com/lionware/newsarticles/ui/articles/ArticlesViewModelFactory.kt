package com.lionware.newsarticles.ui.articles

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

interface ArticlesViewModelFactory : ViewModelProvider.Factory

@Suppress("UNCHECKED_CAST")
class ArticlesViewModelFactoryImpl(
    private val getArticlesUseCase: GetArticlesUseCase
) : ArticlesViewModelFactory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ArticlesViewModel(getArticlesUseCase) as T
    }
}