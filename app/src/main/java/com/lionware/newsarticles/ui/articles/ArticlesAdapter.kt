package com.lionware.newsarticles.ui.articles

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.lionware.newsarticles.data.entities.Article
import com.lionware.newsarticles.databinding.ListItemArticleBinding

class ArticlesAdapter : RecyclerView.Adapter<ArticleViewHolder>() {

    private val items = ArrayList<Article>()

    fun setItems(items: ArrayList<Article>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val binding: ListItemArticleBinding =
            ListItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount(): Int = items.size
}

class ArticleViewHolder(private val itemBinding: ListItemArticleBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(article: Article) {
        itemBinding.source.text = article.source.name
        itemBinding.title.text = article.title
        itemBinding.description.text = article.description
        Glide.with(itemBinding.root)
            .load(article.urlToImage)
            .into(itemBinding.image)
    }
}