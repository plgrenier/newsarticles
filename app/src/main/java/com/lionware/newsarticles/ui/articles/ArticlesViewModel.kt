package com.lionware.newsarticles.ui.articles

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lionware.newsarticles.data.entities.Article
import com.lionware.newsarticles.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ArticlesViewModel constructor(
    private val getArticlesUseCase: GetArticlesUseCase
): ViewModel() {

    val articles: LiveData<Resource<List<Article>>>
        get() = getArticlesUseCase.articles

    fun fetchArticles(query: String, from: String, sortBy: String) {
        viewModelScope.launch(Dispatchers.IO) {
            getArticlesUseCase.get(query, from, sortBy)
        }
    }
}