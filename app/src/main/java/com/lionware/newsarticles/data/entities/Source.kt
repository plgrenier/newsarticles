package com.lionware.newsarticles.data.entities

data class Source(
    val id: String?,
    val name: String
)
