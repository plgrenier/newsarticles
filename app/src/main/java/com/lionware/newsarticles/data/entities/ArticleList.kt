package com.lionware.newsarticles.data.entities

data class ArticleList(
    val info: Info,
    val articles: List<Article>
)
