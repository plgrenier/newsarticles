package com.lionware.newsarticles.data.remote

import com.lionware.newsarticles.data.entities.ArticleList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    @GET("everything")
    suspend fun getArticles(
        @Query("q") q: String,
        @Query("from") from: String,
        @Query("sortBy") sortBy: String
    ): Response<ArticleList>

    companion object {
        const val BASE_URL = "http://newsapi.org/v2/"
    }
}