package com.lionware.newsarticles.data.entities

data class Info(
    val status: String,
    val totalResults: Int
)
