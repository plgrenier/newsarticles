package com.lionware.newsarticles.data.remote

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NewsRepository @Inject constructor(
    private val newsService: NewsService
) : BaseDataSource() {

    suspend fun getArticles(q: String, from: String, sortBy: String) =
        getResult { newsService.getArticles(q, from, sortBy) }
}