package com.lionware.newsarticles.ui.articles

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lionware.newsarticles.CoroutinesTestRule
import com.lionware.newsarticles.R
import com.lionware.newsarticles.data.entities.Article
import com.lionware.newsarticles.data.entities.ArticleList
import com.lionware.newsarticles.data.entities.Info
import com.lionware.newsarticles.data.remote.NewsRepository
import com.lionware.newsarticles.utils.Resource
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GetArticlesUseCaseTest {

    private lateinit var appContext: Context
    private lateinit var newsRepository: NewsRepository
    private lateinit var getArticlesUseCase: GetArticlesUseCase

    private val query = "bitcoin"
    private val from = "2021-01-15"
    private val sortBy = "publishedAt"
    private val dummyArticleList = ArticleList(
        Info("OK", 1000),
        ArrayList<Article>()
    )

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        appContext = mockk<Context>(relaxed = true)
        newsRepository = mockk<NewsRepository>()
    }

    @Test
    fun `given a successful api call, when the articles are fetched, the livedata posts the articles`() = runBlocking {
        // Given
        val observer = createArticlesObserver()
        getArticlesUseCase = GetArticlesUseCase(appContext, newsRepository)
        getArticlesUseCase.articles.observeForever(observer)
        coEvery { newsRepository.getArticles(query, from, sortBy) } answers {
            Resource.success(dummyArticleList)
        }

        // When
        getArticlesUseCase.get(query, from, sortBy)

        // Then
        coVerify { newsRepository.getArticles(query, from, sortBy) }
        verifySequence {
            observer.onChanged(Resource.loading())
            observer.onChanged(Resource.success(dummyArticleList.articles))
        }
    }

    @Test
    fun `given a failed api call, when the articles are fetched, the livedata posts the error`() = runBlocking {
        // Given
        val observer = createArticlesObserver()
        getArticlesUseCase = GetArticlesUseCase(appContext, newsRepository)
        getArticlesUseCase.articles.observeForever(observer)
        coEvery { newsRepository.getArticles(query, from, sortBy) } returns Resource.error("")

        // When
        getArticlesUseCase.get(query, from, sortBy)

        // Then
        coVerify { newsRepository.getArticles(query, from, sortBy) }
        verifySequence {
            observer.onChanged(Resource.loading())
            observer.onChanged(Resource.error(appContext.getString(R.string.network_error)))
        }
    }

    private fun createArticlesObserver(): Observer<Resource<List<Article>>> =
        mockk { every { onChanged(any()) } just Runs }
}